# Frontend System Requirements
Node version: 18.15.0
NPM version: 9.5.0

# Instructions
- Run ```npm install```.
- Run ```npm run dev``` for serving it locally.

# Important information
The frontend app expects to read the fruit data from the Symfony backend which should also be hosted locally.
It expects it at 127.0.0.1:8000 so if it runs somewhere else please change line 97 at ```src/components/FruitList.vue``` accordingly.