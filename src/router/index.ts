import { createRouter, createWebHistory, type RouteRecordRaw } from 'vue-router'
import FruitListPage from '@/views/FruitList.vue'
import FavoritesPage from '@/views/FavoritesList.vue'

const BASE_URL = import.meta.env.BASE_URL
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'FruitListPage',
    component: FruitListPage
  },
  {
    path: '/favorites',
    name: 'FavoritesPage',
    component: FavoritesPage
  }
]

const router = createRouter({
  history: createWebHistory(BASE_URL),
  routes
})

export default router
