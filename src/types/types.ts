export interface Fruit {
  id: string
  name: string
  genus: string
  family: string
  fruit_order: string
  carbohydrates: number
  protein: number
  fat: number
  calories: number
  sugar: number
}
